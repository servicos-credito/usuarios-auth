<?php

namespace App\Http;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class Request extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    // /**
    //  * {@inheritdoc}
    //  */
    // protected function formatErrors(Validator $validator)
    // {
    //     return ['errors' => ['messages' => $validator->errors()->keys()]];
    // }

    protected function isUpdate()
    {
        return in_array($this->method(), ['PATCH', 'PUT']);
    }

    protected function getId()
    {
        return $this->input('id') ?: ($this->route('id') ?: null);
    }
}
