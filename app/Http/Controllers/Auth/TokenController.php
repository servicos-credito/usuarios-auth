<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests;

class TokenController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['store']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Requests\Auth\LoginRequest $request)
    {
        $username_input = $request->input('username', '');
        $credentials = [
            'email' => strtolower($username_input),
            'password' => $request->input('password'),
            'status' => true,
        ];

        if (!$token = auth()->attempt($credentials)) {
            return $this->respondNotFound([], 'Email e/ou senha incorretos');
        }

        $user = auth()->user();

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function whoami()
    {
        $user = auth()->user();

        if (!$user) {
            return $this->respondNotFound();
        }

        return $this->respond($user);
    }

    // /**
    //  * Refresh a token.
    //  *
    //  * @return \Illuminate\Http\JsonResponse
    //  */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    // /**
    //  * Log the user out (Invalidate the token).
    //  *
    //  * @return \Illuminate\Http\JsonResponse
    //  */
    public function revoke()
    {
        auth()->invalidate();

        return $this->respondAccepted();
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ]);
    }
}
