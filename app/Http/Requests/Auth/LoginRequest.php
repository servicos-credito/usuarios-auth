<?php

namespace App\Http\Requests\Auth;

use App\Http\Request;

class LoginRequest extends Request
{
    public function rules()
    {
        return [
            'username' => ['required', 'email', 'string'],
            'password' => ['required', 'string'],
        ];
    }
}
