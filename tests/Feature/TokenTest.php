<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\User;

class TokenTest extends TestCase
{
    use RefreshDatabase;

    protected $user;

    protected function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create(['email' => 'testuser@test.com', 'status' => true]);
    }

    /**
     * @test
     */
    public function an_active_user_should_be_able_to_login()
    {
        $response = $this->json(
            'POST',
            route('auth.token.store'),
            ['username' => $this->user->email, 'password' => 'secret']
        );

        $response->assertStatus(200)
            ->assertJson([
                'access_token' => true,
            ]);
    }

    /**
     * @test
     */
    public function an_inactive_user_should_be_able_to_login()
    {
        $this->user->status = false;
        $this->user->save();

        $response = $this->json(
            'POST',
            route('auth.token.store'),
            ['username' => $this->user->email, 'password' => 'secret']
        );

        $response->assertStatus(404);
    }

    /**
     * @test
     */
    public function incorrect_credentials_should_trigger_not_found()
    {
        $response = $this->json(
            'POST',
            route('auth.token.store'),
            ['username' => $this->user->email, 'password' => 'wrong-secret']
        );

        $response->assertStatus(404);
    }

    /**
     * @test
     */
    public function missing_credentials_should_trigger_unprocessable()
    {
        $response = $this->json(
            'POST',
            route('auth.token.store'),
            ['username' => $this->user->email]
        );

        $response->assertStatus(422);
    }

    /**
     * @test
     */
    public function an_valid_token_should_belong_to_a_user()
    {
        $token = auth()->login($this->user);

        $response = $this->withHeaders(['Bearer' => $token,])
            ->json('GET', route('auth.token.whoami'));

        $response->assertStatus(200)
             ->assertJson([
                'email' => true,
            ]);
    }

    /**
     * @test
     */
    public function an_valid_token_can_be_refreshed()
    {
        $token = auth()->login($this->user);

        $response = $this->withHeaders(['Bearer' => $token,])
            ->json('POST', route('auth.token.refresh'));

        $response->assertStatus(200)
             ->assertJson([
                'access_token' => true,
            ]);
    }
}
