<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {
    /**
     *  Auth // Activation // Reset Password Routes
     */
    Route::group(['namespace' => 'Auth', 'as' => 'auth.'], function () {
        // Token Management (Authentication)
        Route::post('/login', 'TokenController@store')->name('token.store');
        Route::get('/whoami', 'TokenController@whoami')->name('token.whoami');
        Route::post('/refresh', 'TokenController@refresh')->name('token.refresh');
        Route::any('/logout', 'TokenController@revoke')->name('token.revoke');

        // Password Resets
        // Route::post('/password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        // Route::post('/password/reset', 'ResetPasswordController@reset')->name('password.reset');

        // Register
        // Route::post('/signin', 'RegisterController@signUp')->name('register');
    });
});
