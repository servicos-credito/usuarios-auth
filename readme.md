## Desafio Score

### Serviço do Ususario Auth

Esse projeto faz parte do conjunto de servicos para souçõ do problema de Score Crédito porposto

### Requerimentos

- php 7.1+
- banco de dados

Para poder utilziar este serviço é necessário um banco de dados podendo ser postgreSQL, mysql, mariadb, SQL server ou SQLite. Para realizar os testes é necessário a utilização do SQLite.

### Inicializando

Vamos inicar por instalar as dependencias do projeto com o [composer](https://getcomposer.org):

```
$(which composer) install
```

Para iniciar o projeto copie o arquivo `.env.example` na raiz do projeto para `.env` e insira os dados de conexao com o seu banco de dados. Para configurar o banco siga os passos [descritos aqui](https://laravel.com/docs/5.7/database) e verifique que o banco foi criado.

Por exemplo, se utilizar mariaDB com uma configuração canônica:

```
$ mysql -uroot -p
> create database score_usuarios_auth;
> exit;
```

e no arquivo `.env` com:

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=score_usuarios_auth
DB_USERNAME=root
DB_PASSWORD=<<sua_senha>>
```

Agora vamos gerar as chaves de segredo para utiliziação da aplicação com:

```
php artisan key:generate
php artisan jwt:secret
```

E por fim vamos popular nosso banco de dados com alguns dados fictícios

```
php artisan db:migrate --seed
```

Tudo pronto.

### Testando

#### Postman

Para realizar o teste sem configurar um domínio pode-se utilizar o servidor interno do php com o wrapper fornecido pelo comando `artisan`:

```
php artisan --port=8001
```

Para testar é possível utilizar a [coleção do Postman](https://www.getpostman.com) fornecida em: [https://documenter.getpostman.com/view/5462552/RWaRP5fz](https://documenter.getpostman.com/view/5462552/RWaRP5fz).

Basta atualizar as variaveis de ambiente da ferramenta para a URL utilziada em sua máquina. No caso de utlizar o servidor interno, atualizar a variavel *USUARIOS_AUTH_URL* para `127.0.0.1/api`

#### Testes automatizados

Para forncer maior robustez e confiablidade ao serviço foram inseridos testes de integração que podem ser encontados na pasta `tests`. Para rodar os testes automátizados basta utilizar o `phpunit` (instalado junto com as dependencias de dev) no terminal:

```
$(which php) vendor/bin/phpunit
```

* Lembrando que nào é necessário iniciar o servidor para executar os estes testes